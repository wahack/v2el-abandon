[TOC]

# 进程管理Supervisor
## 安装
ubuntu环境下: `sudo apt-get install supervisor`

安装成功后默认产生的文件结构
```
	/usr/bin
		supervisord 	  # 可执行文件,启动服务进程
		supervisorctl     # 可执行文件,启动命令行
	/etc/supervisor
		conf.d 			  # 目录，存放supervisor监管的进程配置文件，一个进程一个配置文件
		supervisor.conf   # 文件，主配置文件
```

## 配置
- 实现对一个进程的监控
在`/etc/supervisor/conf.d`目录下新建对该进程的配置文件`filename.conf`
```
	[program:script.py]
	command = /home/hadoop/sdf.py	# 启动程序的命令
	autorstart = true               # 设置程序是否随supervisor的启动而启动
	directory = 				    # 在该目录下运行程序
	autorestart = true			    # 程序停止后是否重新将其启动
	startsecs = 				    # 启动时等待的时间
	startretries = 				    # 重启程序的次数
	redirect_stderr = true			# 是否将程序错误信息重定向到文件
	stdout_logfile = /home/hadoop/supervisor_log/log.txt  #将程序输出重定向到该文件
	stderr_logfile = /home/hadoop/supervisor_log/err.txt  #将程序错误信息重定向到该文件
```
**修改supervisord.conf后要执行以下命令使配置文件生效**
- `supervisorctl update`
- 或 `supervisorctl reload`


还可以开启基于http的web控制台

修改主配置文件 
vim /etc/supervisor/supervisord.conf，添加如下内容：
```
[inet_http_server]
port = 192.168.1.60:9001 ＃IP和绑定端口
username = admin ＃管理员名称
password = 123456 #管理员密码
```

##启动
- 启动supervisor `supervisord`
- 打开命令行 `supervisorctl`

-----------------------------------------------------
# 存储数据库MongoDB
## ubuntu环境下的安装与配置

### 安装

1. `sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10`
2. `echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list`
3. `sudo apt-get update`
4. `sudo apt-get install mongodb-org`

详情见[官方文档](http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/)

### 启动与配置

- 数据库配置文件mongodb.conf
```
	systemLog:
	    destination: file
	    path: '/var/log/mongodb/v2el.log'
	    quiet: true
	    logAppend: true

	net:
		bindIp: 127.0.0.1
		port: 27017

	storage:
		dbPath: '/data/db'
```
更多配置参数详见[官方文档](http://docs.mongodb.org/manual/reference/program/mongod/#bin.mongod)

- 按配置启动`mongod --config mongodb.conf`


-----------------------------------------------------------------
# 缓存数据库redis
## ubuntu环境下的启动与配置