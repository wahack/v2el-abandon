var gulp = require('gulp');
// var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var livereload = require('gulp-livereload');
var sass = require('gulp-sass');

// var inject = require('gulp-inject');
// var multinject = require('gulp-multinject');


var config = {
	'app': './webapp-dev/',
	'dist': './webapp/'
};

var basicJs = [
	 './webapp-dev/bower_components/jquery/dist/jquery.js',
	 './webapp-dev/bower_components/underscore/underscore.js',
	 './webapp-dev/bower_components/backbone/backbone.js',
	 './webapp-dev/bower_components/requirejs/require.js'
];


// var basicCss = './webapp-dev/bower_components/foundation/css/foundation.css';

console.log('gulpfile run ...');

//  //语法检查
// gulp.task('jshint', function(){
// 	console.log('jshint task run .......');
// 	return gulp.src(config.app+'scripts/**/*.js')
// 		.pipe(jshint())
// 		.pipe(jshint.reporter('default'));
// });

//合并文件
gulp.task('concat', function(){
  	return gulp.src( basicJs )
  		.pipe(concat('basic.js'))
  		.pipe(gulp.dest(config.app + 'scripts/'));
});

//编译sass文件至css
gulp.task('sass', function(){
	return gulp.src( config.app + 'styles/**/*.scss' )
		.pipe(sass())
		.pipe(rename(function(path){
			path.dirname =  path.dirname.substring(0,path.dirname.indexOf('\\scss'));
		}))
		.pipe(gulp.dest( config.app + 'styles'));
});

// 压缩代码
gulp.task('minify', function(){
	return gulp.src()
		.pipe()
		.pipe(gulp.dest(config.app.scripts));
});

// 监视文件
gulp.task('watch', function(){
	livereload.listen();
	gulp.watch( config.app + 'styles/**/*.scss', function(){
		gulp.run('sass');
	});
	gulp.watch( config.app + '**').on('change', livereload.changed);
});

gulp.task('init', ['concat']);
gulp.task('default', ['watch', 'sass']);
gulp.task('deploy', ['minify']);
